import React from "react";
import {Link} from "react-router-dom";
import "./Navigation.css";

/*
<Link to={{ pathname: "/about?fromNavigation=true"}}>About</Link> 이런식으로 파라미터 붙치는건 별로 안좋은 방식. 아래처럼 하자
<Link to={{ pathname: "/about", state: { fromNavigation: true } }}>About</Link>
*/

function Navigation() {
    return (
        <div className="nav">
            <Link to="/">Home</Link>
            <Link to="/about">About</Link>
        </div>
    );
}

export default Navigation;