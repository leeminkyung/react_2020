import React from 'react';
import {Link} from "react-router-dom";
import PropTypes, { bool } from 'prop-types';
import "./Movie.css"

// component가 state가 필요없을 경우에는 class component가 될 필요가 없어서 function component로 제작하겠음.
function Movie({id, year, title, summary, poster, genres}){
    return (
        <div className="Movie">
            <Link
                to={{
                    pathname: `/movie/${id}`,
                    state: { year, title, summary, poster, genres }
                }}
            >
                <img src={poster} alt={title} title={title} className="Movie__Poster" />
                <div className="Movie__Column">
                    <h1 className="movie__title" style={{fontWeight: "bold"}}>{title}</h1>
                    <h5 className="movie__year">{year}</h5>
                    <ul className="movie__genres">
                        {genres.map((genre, index) => (
                        <li key={index} className="genres__genre">
                            {genre}
                        </li>
                        ))}
                    </ul>
                    <p className="movie__summary">{summary.slice(0,140)}</p>
                </div>
            </Link>
        </div>
    )
}

Movie.propTypes = {
    id: PropTypes.number.isRequired,
    year: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
    poster: PropTypes.string.isRequired,
    genres: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default Movie;