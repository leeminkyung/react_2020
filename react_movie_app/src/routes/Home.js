import React from 'react';
import axios from 'axios';
import Movie from '../components/Movie';
import "./Home.css"

class Home extends React.Component {
  state = {
    isLoading: true,
    movies: []
  };
  getMovies = async () => {
    // async 는 js의 비동기 함수. (async : 너는 이걸 기다려야해,  await : 내가 뭘 기다리길 원해? axios? 그래! 기다릴게!)
    const {
      data: {
        data: {movies}
      }
    } = await axios.get("https://yts-proxy.now.sh/list_movies.json?sort_by=rating");
    this.setState({movies, isLoading: false});
  }
  componentDidMount() {
    this.getMovies();
  }
  render() {
    // javascript class 안에 있으면 component class 에 의해서 헤깔려함. 그래서 className을 사용해야함.
    const {isLoading, movies} = this.state;
    return (
      <section className="container">
        {isLoading ? (
          <div className="loader">
            <span className="App--loading">Loading...</span>
          </div>
        ) : (
          <div className="App">
            {movies.map(val => (
              <Movie 
                key={val.id} 
                id={val.id} 
                year={val.year} 
                title={val.title} 
                summary={val.summary} 
                poster={val.medium_cover_image}
                genres={val.genres}
              />
            ))}
          </div>
        )}
      </section>
    );
  }
}

export default Home;
