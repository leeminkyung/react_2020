import React from "react";

class Detail extends React.Component {
    componentDidMount() {
        const { location, history } = this.props;
        if (location.state === undefined) {
            // detail 페이지를 list에서 클릭하지 않고, URL을 쳐서 들어오면 리다이렉션 시킴
            history.push('/');
        }
    }
    render() {
        // render 동작 후 componentDidMount이 동작을 하니깐 location.state 체크 해서 없으면 null return 해줌.
        const { location } = this.props;
        if (location.state) {
            return <span>{location.state.title}</span>;
        } else {
            return null;
        }
    }
}

export default Detail;